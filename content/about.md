---
title: "About"
date: 2022-03-28T12:42:36+02:00
draft: true
---

Anonymous Strategic Support (A.S.S./Anon S.S.) was created to meet the emerging need for strategic operations during wartime. Through collaboration and co-operation Anon S.S. aims to bring together hackers and activists with diverse backgrounds and experience. Anonymous is a concept that no one person or group can speak on behalf of. Anon S.S. does not speak for or claim the actions of all Anons. Our core mission is to defend the internet rights and freedoms that we believe in; the right to anonymity, the right to information and the right to access the internet. Each operation has its own objectives and the scale and scope of our responses vary in proportion to the challenges we are faced with. The members of Anon S.S. are committed to seeing our operations through to the end. 

Expect us.
